<?php

namespace App\Presenters;

use Nette,
    App\Model;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter {

    /** @var \App\Rpj\Forms\SignUpForm @inject */
    public $signUpFormFactory;

    //sign form is defined in base presenter

    protected function createComponentSignUpForm() {
        $form = $this->signUpFormFactory->create();

        $presenter = $this;
        
        $form->onSuccess[] = function (\Nette\Application\UI\Form $form) use ($presenter) {
            if ($presenter->signUpFormFactory->signUpResponse == 'FAIL') {
                $presenter->flashMessage('Somethign went wrong! Please try other login info or try again later.', 'alert');
                $presenter->redirect('Sign:up');
            }
            if ($presenter->signUpFormFactory->signUpResponse == 'SUCCESS') {
                $presenter->redirect('Sign:edUp');
            }
        };

        return $form;
    }

    public function actionOut() {
        $this->getUser()->logout();
        $this->flashMessage('You have been signed out.');
        $this->redirect('in');
    }

}
