<?php

namespace App\Presenters;

use Nette;

/**
 * Sql presenter.
 */
class SqlPresenter extends AbstractPagesPresenter {

    protected function init() {
        $this->tag = 'sql';
        $this->title = 'SQL syntax';
    }

}
