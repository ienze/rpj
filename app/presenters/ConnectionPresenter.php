<?php

namespace App\Presenters;

use Nette;

/**
 * Connection presenter.
 */
class ConnectionPresenter extends AbstractPagesPresenter {

    protected function init() {
        $this->tag = 'connection';
        $this->title = 'Pripojenie k databáze';
    }

}
