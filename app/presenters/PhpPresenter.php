<?php

namespace App\Presenters;

use Nette;

/**
 * Php presenter.
 */
class PhpPresenter extends AbstractPagesPresenter {

    protected function init() {
        $this->tag = 'php';
        $this->title = 'PHP';
    }

    public function renderPriklady($page) {
        $this->tag = 'priklad';
        $this->title = 'PHP > Priklady';
        $this->renderDefault($page);
    }

}
