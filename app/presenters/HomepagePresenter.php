<?php

namespace App\Presenters;

use Nette,
    Nette\Application\Responses\JsonResponse;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends AbstractPagesPresenter {

    /** @var \App\Rpj\TagTextProcessor @inject */
    public $tagTextProcessor;

    protected function init() {
        $this->tag = 'home';
        $this->title = '';
        $this->pagesTemplate = 'Homepage/default.latte';
    }

    public function renderShow($pageId) {

        
        if (Nette\Utils\Validators::isNumericInt($pageId)) {
            $page = $this->database->table('mysql_pages')->get($pageId);
        } else {
            $page = $this->database->table('mysql_pages')->where('slug', $pageId)->fetch();
        }

        if (!$page) {
                $this->error('Page not found');
            }
        
        //download row to stdClass
        $page = (object) $page->toArray();

        //process tags
        $page->content = $this->tagTextProcessor->processTags($this, $this->database, $page->content);

        if (!$this->isAjax()) {

            $this->template->page = $page;

            $this->template->title = $page->title;
            $this->template->description = $page->excerpt;
            $this->template->keywords = $this->getPageTagsPublicString($page->id, true);

            $this->template->tags = $this->getPageTagsPublicString($page->id);
        } else {

            $this->sendResponse(new JsonResponse(array(
                'title' => $page->title,
                'content' => $page->content
            )));
        }
    }

    public function actionRandom() {

        $random = $this->database->table('mysql_pages')
                ->order(new \Nette\Database\SqlLiteral('RAND()'))
                ->limit(1)
                ->fetch();

        $this->redirect('Homepage:show', $random->slug);
    }

    public function renderTag($tag, $page) {

        $this->tag = $tag;

        $this->template->title = $tag;

        $this->renderDefault($page);
    }

    public function getPageTagsPublicString($pageId, $plain = false) {

        if ($plain) {
            $tagsString = '';
        } else {
            $tagsString = '<ul class="inline-list">';
        }
        $tags = $this->getPageTags($pageId);

        foreach ($tags as $tag) {

            if (Nette\Utils\Strings::startsWith($tag, '_')) {
                continue;
            }

            if ($plain) {
                if ($tagsString) {
                    $tagsString .= ', ';
                }
                $tagsString .= $tag;
            } else {
                $tagsString .= '<li><a href="' . $this->link('Homepage:tag', $tag) . '">' . $tag . '</a></li>';
            }
        }

        if (!$plain) {
            $tagsString .= '</ul>';
        }

        return $tagsString;
    }

    public function renderSearch($search, $page) {

        if (!$page) {
            $page = 1;
        }

        $likeSearch = '%'.$search.'%';

        $query = $this->database->table('mysql_pages')
            ->where('title LIKE ? OR content LIKE ?', array($likeSearch, $likeSearch));
        
        $count = $query->count();
        
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($count);
        $paginator->setItemsPerPage($this->settings->items_per_page);
        $paginator->setPage($page);

        $this->template->paginator = $paginator;

        if ($count > 0) {

            $this->template->pages = $query->limit($paginator->length, $paginator->offset);
        } else {
            $this->template->pages = NULL;
        }

        $this->template->setFile(__DIR__ . '/../templates/' . 'AbstractPages/default.latte');
        $this->template->title = $this->title;
    }

}
