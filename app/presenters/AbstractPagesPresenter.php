<?php

namespace App\Presenters;

use Nette,
    App\Model;

abstract class AbstractPagesPresenter extends BasePresenter {

    protected $tag;
    protected $title;
    protected $pagesTemplate;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;

        $this->pagesTemplate = 'AbstractPages/default.latte';

        $this->init();
    }

    abstract protected function init();

    public function renderDefault($page) {

        if (!$page) {
            $page = 1;
        }

        $pagesQuery = $this->database->table('mysql_pages')
                ->where("order > ?", 0)
                ->order('order DESC');

        if ($this->tag) {
            $tagId = $this->getTagId($this->tag, false);

            $pagesQuery->where(":mysql_pages_tags.page_id = id")
                    ->where(":mysql_pages_tags.tag_id = ?", $tagId);
        }


        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($pagesQuery->count());
        $paginator->setItemsPerPage($this->settings->items_per_page);
        $paginator->setPage($page);

        $this->template->paginator = $paginator;

        if ($pagesQuery->count() > 0) {
            $this->template->pages = $pagesQuery->limit($paginator->length, $paginator->offset);
        } else {
            $this->template->pages = NULL;
        }

        $this->template->setFile(__DIR__ . '/../templates/' . $this->pagesTemplate);
        $this->template->title = $this->title;
    }

}
