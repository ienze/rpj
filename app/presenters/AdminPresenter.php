<?php

namespace App\Presenters;

use Nette,
    App\Rpj\Role;

/**
 * AdminPresenter
 */
class AdminPresenter extends BasePresenter {

    protected function beforeRender() {
        if (!$this->user->isLoggedIn()) {
            $this->redirect('Homepage:');
        }

        $this->template->permissionDenyed = false;
        $this->template->adminArea = true;

        parent::beforeRender();
    }

    /*
      PAGES
     */

    public function renderDefault($page, $tag) {

        if (!$this->user->isAllowed("pages", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }

        if (!$page)
            $page = 1;

        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemsPerPage(30);
        $paginator->setPage($page);

        $itemsQuery = $this->database->table('mysql_pages');

        if ($tag) {
            $tagId = $this->getTagId($tag, false);

            $itemsQuery->where(":mysql_pages_tags.page_id = id")
                    ->where(":mysql_pages_tags.tag_id = ?", $tagId);
        }

        $paginator->setItemCount($itemsQuery->count());

        $this->template->paginator = $paginator;

        $this->template->pages = $itemsQuery->limit($paginator->getLength(), $paginator->getOffset());
    }

    public function renderAdd() {

        if (!$this->user->isAllowed("pages", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }

        $this->template->setFile(__DIR__ . '/../templates/Admin/edit.latte');
    }

    public function renderEdit($pageId) {

        if (!$this->user->isAllowed("pages", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }

        $page = $this->database->table('mysql_pages')->get($pageId);
        if ($page) {

            $vals = $page->toArray();

            $vals['tags'] = $this->getPageTagsString($page->id);

            $this['pageForm']->setDefaults($vals);
        }
    }

    public function getPageTagsString($pageId) {

        $tagsString = '';
        $tags = $this->getPageTags($pageId);

        foreach ($tags as $tag) {
            if ($tagsString) {
                $tagsString .= ', ';
            }
            $tagsString .= $tag;
        }

        return $tagsString;
    }

    protected function createComponentPageForm() {
        $form = new Nette\Application\UI\Form;

        $form->addText('title', 'Titulok:')
                ->setRequired();

        $form->addTextArea('content', 'Obsah:')
                ->setRequired()
                ->setAttribute('class', 'mceEditor');

        $form->addTextArea('excerpt', 'Zhrnutie:')
                ->setRequired();

        $form->addText('order', 'Poradie zobrazenia článku:')
                ->setRequired();

        $form->addText('tags', 'Tagy:')
                ->setRequired();

        $form->addSubmit('send', 'Uložiť');

        $form->onSuccess[] = $this->pageFormSucceeded;

        return $form;
    }

    public function pageFormSucceeded($form) {

        if (!$this->user->isAllowed("pages", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }

        $values = $form->getValues();
        $pageId = $this->getParameter('pageId');

        $tagsString = $values['tags'];
        unset($values['tags']);

        $values['slug'] = Nette\Utils\Strings::webalize($values['title']);

        if ($pageId) {
            $this->database->table('mysql_pages')->get($pageId)->update($values);
        } else {
            $insertedPage = $this->database->table('mysql_pages')->insert($values);

            $pageId = $insertedPage->id;
        }

        if ($pageId) {

            $tags = explode(',', $tagsString);

            $this->database->table('mysql_pages_tags')->where('page_id', $pageId)->delete();

            foreach ($tags as $tag) {

                $tag = trim($tag);

                $insertedPage = $this->database->table('mysql_pages_tags')->insert(array(
                    'page_id' => $pageId,
                    'tag_id' => $this->getTagId($tag)
                ));
            }
        }

        $this->flashMessage('Článok bol uložený', 'success');
        $this->redirect('Admin:default');
    }

    protected function createComponentTagSelectForm() {
        $form = new Nette\Application\UI\Form;

        $form->addText('tag', 'Tag:')
                ->setRequired();

        $form->addSubmit('send', 'Filtrovať');

        $form->onSuccess[] = $this->tagSelectFormSucceeded;

        return $form;
    }

    public function tagSelectFormSucceeded($form) {

        $page = $this->getParameter('page');
        if (!$page) {
            $page = 1;
        }

        $tag = $form->values['tag'];

        $this->redirect('Admin:default', $page, $tag);
    }

    public function actionDelete($pageId) {

        if (!$this->user->isAllowed("pages", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }

        $this->database->table('mysql_pages')->where('id = ?', $pageId)->delete();
        $this->redirect('Admin:default');
    }

    /*
      SETTINGS
     */

    public function renderSettings() {

        if (!$this->user->isAllowed("settings", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }
    }

    protected function createComponentSettingsForm() {
        $form = new Nette\Application\UI\Form;

        foreach ($this->settings->all() as $setting) {
            $form->addText($setting->setting, $setting->name)
                    ->setValue($setting->value);
        }

        $form->addSubmit('send', 'Uložiť');

        $form->onSuccess[] = $this->pageSettingsSucceeded;

        return $form;
    }

    public function pageSettingsSucceeded($form) {

        if (!$this->user->isAllowed("settings", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }

        $values = $form->getValues();

        foreach ($this->settings->all() as $setting) {
            $settingName = $setting->setting;
            $this->settings->$settingName = $values[$settingName];
        }
    }

    /*
      USERS
     */

    public function renderUsers($page) {

        if (!$this->user->isAllowed("users", "edit")) {
            $this->template->permissionDenyed = true;
            return;
        }

        if (!$page) {
            $page = 1;
        }

        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemsPerPage(30);
        $paginator->setPage($page);

        $itemsQuery = $this->database->table('mysql_users');

        $paginator->setItemCount($itemsQuery->count());

        $this->template->paginator = $paginator;

        $this->template->users = $itemsQuery->limit($paginator->getLength(), $paginator->getOffset());
    }

    public function renderUsersAdd() {

        if (!$this->user->isAllowed("users", "add")) {
            $this->template->permissionDenyed = true;
            return;
        }
    }

    public function renderUsersEdit($userId) {

        $editUser = $this->database->table('mysql_users')->get($userId);

        if (!$this->user->isAllowed("users", "edit") || !$editUser) {
            $this->template->permissionDenyed = true;
            return;
        }

        $userPower = Role::rolePower($this->getUser()->getIdentity()->role);
        $editUserPower = Role::rolePower($editUser->role);

        if ($userPower > $editUserPower) {
            $this['userForm']->setDefaults($editUser->toArray());
        } else {
            $this->template->permissionDenyed = true;
            return;
        }
    }

    protected function createComponentUserForm() {
        $form = new Nette\Application\UI\Form;

        $form->addSelect('role', 'Rola', Role::rolesForFormSelect())
                ->setRequired();

        $form->addSubmit('send', 'Uložiť');

        $form->onSuccess[] = $this->formUserSucceeded;

        return $form;
    }

    public function formUserSucceeded($form) {

        $values = $form->getValues();
        $userId = $this->getParameter('userId');

        $editUser = $this->database->table('mysql_users')->get($userId);
        if (!$this->user->isAllowed("users", "edit") || !$editUser) {
            $this->template->permissionDenyed = true;
            return;
        }

        $userPower = Role::rolePower($this->getUser()->getIdentity()->role);
        $editUserPower = Role::rolePower($editUser->role);
        $editPower = Role::rolePower($values->role);

        if ($userPower > $editUserPower &&
            $userPower > $editPower) {
            $editUser->update($values);
        }

        $this->redirect('Admin:users');
    }

    public function actionUsersDelete($userId) {

        $editUser = $this->database->table('mysql_users')->get($userId);
        if (!$this->user->isAllowed("users", "edit") || !$editUser) {
            return;
        }

        $userPower = Role::rolePower($this->getUser()->getIdentity()->role);
        $editUserPower = Role::rolePower($editUser->role);

        if ($userPower > $editUserPower) {
            $editUser->delete();
        }
        $this->redirect('Admin:users');
    }

    /*
      OTHER
     */

    public function actionRegenerateSlugs() {

        $pages = $this->database->table('mysql_pages');

        foreach ($pages as $page) {

            $originalSlug = Nette\Utils\Strings::webalize($page->title);
            $slug = $originalSlug;

            $i = 2;
            while ($this->database->table('mysql_pages')->where('slug', $slug)->fetch()) {
                $slug = $originalSlug . '-' . $i++;
            }

            $page->update(array(
                'slug' => $slug
            ));
        }

        $this->redirect('Admin:');
    }

}
