<?php

namespace App\Presenters;

/**
 * Tools presenter.
 */
class ToolsPresenter extends BasePresenter {

    public function renderPhpmyadmin() {
        
        if (isset($_SESSION['PMA_single_signon_error_message'])) {
            $this->template->pmaError = $_SESSION['PMA_single_signon_error_message'];
            unset($_SESSION['PMA_single_signon_error_message']);
        }
        
    }
    
    public function createComponentPhpmyadminForm() {
        $form = new \Nette\Application\UI\Form;

        $form->addText('user', 'Prihlasovacie meno:')
                ->setRequired('Je potrebne vyplnit prihlasovacie meno.');

        $form->addText('password', 'Heslo:');

        $form->addText('host', 'Host:')
                ->setRequired('Je potrebne vyplnit adresu serveru.');
        
        $form->addText('port', 'Port:')
                ->setAttribute('placeholder', '3306');
        
        $form->addSubmit('send', 'Prihlasit');

        $form->onSuccess[] = $this->phpmyadminFormSucceeded;

        return $form;
    }
    
    public function phpmyadminFormSucceeded($form, $values) {
        
        if(!$values['port']) {
            $values['port'] = '3306';
        }
        
        /* Store there credentials */
        $_SESSION['PMA_single_signon_user'] = $values['user'];
        $_SESSION['PMA_single_signon_password'] = $values['password'];
        $_SESSION['PMA_single_signon_host'] = $values['host'];
        $_SESSION['PMA_single_signon_port'] = $values['port'];
        
        session_write_close();
        
        /* Redirect to phpMyAdmin */
        $basePath = $this->context->httpRequest->url->basePath;
        header('Location: '.$basePath.'phpmyadmin/index.php');
        
    }
}
