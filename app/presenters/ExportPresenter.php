<?php

namespace App\Presenters;

use Nette;

/**
 * Presenter for exporting data in some unspecified format
 */
class ExportPresenter extends BasePresenter {

    function renderDefault() {

        //load all tables
        $this->template->pages = $this->database->table('mysql_pages');
        $this->template->tags = $this->database->table('mysql_tags');
        $this->template->pageTags = $this->database->table('mysql_pages_tags');
    }

}
