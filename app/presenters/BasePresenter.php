<?php

namespace App\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    /** @var \Nette\Database\Context */
    protected $database;

    /** @var \App\Rpj\SettingsLoader @inject */
    public $settings;

    /** @var \App\Rpj\Forms\SignInForm @inject */
    public $signInFormFactory;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    protected function beforeRender() {
        parent::beforeRender();
        $this->template->settings = $this->settings;
    }

    /*
      TAGS
     */

    public function getTagId($tag, $create = true) {

        $tagSel = $this->database->table('mysql_tags')->where('tag', $tag)->fetch();

        if ($tagSel) {
            return $tagSel->id;
        }

        if ($create) {
            $tagIn = $this->database->table('mysql_tags')->insert(array(
                'tag' => $tag
            ));

            if ($tagIn) {
                return $tagIn->id;
            }
        }

        return false;
    }

    public function getPageTags($pageId) {

        $tagsArray = array();

        $tags = $this->database->table('mysql_pages_tags')
                ->where('page_id', $pageId);

        foreach ($tags as $tag) {
            $tagNameQuery = $this->database->table('mysql_tags')->where('id = ?', $tag->tag_id)->fetch();

            $tagsArray[] = $tagNameQuery->tag;
        }

        return $tagsArray;
    }

    /*
      SIGN IN
     */

    public function createComponentSignInForm() {
        $form = $this->signInFormFactory->create();
        
        $presenter = $this;
        
        $form->onSuccess[] = function (\Nette\Application\UI\Form $form) use ($presenter) {
            
            if ($presenter->signInFormFactory->signInResponse == 'SUCCESS') {

                if ($presenter->getUser()->isAllowed('pages', 'edit')) {
                    $presenter->redirect('Admin:');
                } else {
                    $presenter->redirect('Homepage:');
                }
            } else {
                $presenter->redirect('Sign:in');
            }
        };

        return $form;
    }

    /*
      SEARCH
     */
    
    public function createComponentSearchForm() {
        
        $form = new Form();
        $form->addText('search', 'Vyhladavanie:');
        
        $form->addSubmit('send', 'Hľadať')
                ->setAttribute('class', 'button expand');
        
        $presenter = $this;
        
        $form->onSuccess[] = function(Form $form, $values) use ($presenter) {
            $presenter->redirect('Homepage:search', $values['search']);
        };
        
        return $form;
    }
}
