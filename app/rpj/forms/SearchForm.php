<?php

namespace App\Rpj\Forms;

use \Nette\Application\UI\Form;

class SearchForm extends Form {

    public function create() {
        $form = new Form();
        $form->addText('search', 'Vyhladavanie:');
        
        $form->addSubmit('submit', 'Hľadať')
                ->setAttribute('class', 'button expand');
        
        $form->onSuccess[] = $this->formSucceeded;
        return $form;
    }

    public function formSucceeded($form) {
        
    }
}
