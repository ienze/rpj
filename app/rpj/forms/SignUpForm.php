<?php

namespace App\Rpj\Forms;

use \Nette\Application\UI\Form;

class SignUpForm extends Form {

    /** @var App\Model\UserManager */
    protected $userManager;
    public $signUpResponse;

    public function __construct(\App\Rpj\UserManager $userManager) {
        parent::__construct();
        $this->userManager = $userManager;
    }

    public function create() {
        $form = new Form();

        $form->addText('username', 'Username')
                ->addRule(Form::MIN_LENGTH, 'Username requires at least %d characters.', 3)
                ->addRule(Form::MAX_LENGTH, 'Username cant be longer than %d characters.', 16)
                ->addRule(Form::PATTERN, 'Username can contain only letters a-z A-Z and digits 0-9.', '[a-zA-Z0-9]+')
                ->setRequired(true);
        
        /*
        $form->addText('email', 'E-mail', 35)
                ->addRule(Form::EMAIL, 'Neplatná emailová adresa')
                ->setRequired(true);
        */
        
        $form->addPassword('password', 'Password', 64)
                ->addRule(Form::MIN_LENGTH, 'Heslo musi mat aspon %d znakov!', 8)
                ->setRequired(true);

        $form->addPassword('password2', 'Repeat password', 64)
                ->addRule(Form::EQUAL, 'Passwords doesnt match.', $form['password'])
                ->setRequired(true);

        $form->addSubmit('send', 'Sign up');

        $form->onSuccess[] = $this->formSucceeded;

        return $form;
    }

    public function formSucceeded(Form $form) {

        $values = $form->getValues();

        $new_user = $this->userManager->add($values["username"], $values["password"]);
        if ($new_user) {
            $this->signUpResponse = 'SUCCESS';
        } else {
            $this->signUpResponse = 'FAIL';
        }
    }

}
