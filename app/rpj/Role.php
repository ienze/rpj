<?php

namespace App\Rpj;

class Role extends \Nette\Object implements \Nette\Security\IAuthorizator {

    const
            GUEST = "guest",
            USER = "user",
            MODERATOR = "moderator",
            ADMIN = "admin";

    public static function rolePower($role) {

        $rolePowers = array(
            "guest" => 0,
            "user" => 1,
            "moderator" => 2,
            "admin" => 4,
        );

        return $rolePowers[$role];
    }

    public static function rolesForFormSelect() {
        return array(
            self::USER => 'Užívateľ',
            self::MODERATOR => 'Moderátor',
            self::ADMIN => 'Administrátor'
        );
    }

    private $permissions;

    public function __construct() {
        $perms = new \Nette\Security\Permission();

        $perms->addRole("guest");
        $perms->addRole("user", "guest");
        $perms->addRole("moderator", "user");
        $perms->addRole("admin", "moderator");

        $perms->addResource('tags');
        $perms->addResource('pages');
        $perms->addResource('comments');
        $perms->addResource('users');
        $perms->addResource('settings');

        $perms->allow("guest", "tags", "view");
        $perms->allow("guest", "pages", "view");
        $perms->allow("guest", "comments", "view");
        //TODO is settings->allow guests write comments... $perms->allow("guest", "comments", "add");

        $perms->allow("user", "comments", "add");

        $perms->allow("moderator", "pages", "edit");
        $perms->allow("moderator", "comments", "edit");
        $perms->allow("moderator", "users", "edit");
        $perms->allow("moderator", "settings", "edit");

        $perms->allow('admin', \Nette\Security\Permission::ALL, array('view', 'edit', 'add'));

        $this->permissions = $perms;
    }

    public function createAuthorizator() {
        return $this;
    }

    function isAllowed($role, $resource, $privilege) {
        return $this->permissions->isAllowed($role, $resource, $privilege);
    }

}
