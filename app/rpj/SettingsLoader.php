<?php

namespace App\Rpj;

use Nette;

/**
 * SettingsLoader.
 */
class SettingsLoader {

    /** @var Nette\Database\Context */
    private $database;
    private $settings;
    private $settingsNames;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
        $this->settings = new Nette\ArrayHash();

        $this->settingsNames = array(
            'titulok' => 'Titulok',
            'items_per_page' => 'Počet článkov na stranu',
            'description' => 'Popis',
            'keywords' => 'Kľúčové slová');
    }

    public function __get($name) {
        return $this->get($name);
    }

    public function get($name) {

        if (!isset($this->settings[$name])) {

            $val = $this->database->table('mysql_settings')->get($name);

            if ($val) {
                $this->settings[$name] = $val->value;
            } else {
                $this->settings[$name] = null;
            }
        }

        return $this->settings[$name];
    }

    public function __set($name, $value) {
        $this->set($name, $value);
    }

    public function set($setting, $value) {
        $val = $this->database->table('mysql_settings')->get($setting);
        if ($val) {
            $val->update(array('value' => $value));
        } else {
            $this->database->table('mysql_settings')->insert(array('setting' => $setting, 'value' => $value));
        }
    }

    public function all() {

        $out = array();

        foreach ($this->settingsNames as $setting => $name) {
            $out[] = (object) array('setting' => $setting, 'value' => $this->get($setting), 'name' => $name);
        }

        return $out;
    }

}
