<?php

namespace App\Rpj;

use Nette\Utils\Strings,
    Nette\Object;

class TagTextProcessor extends Object {

    const HASHTAG_REGEX = '/(\s|>)#((\w|\\[|\\]|\;)*+)/';

    public function processTags($presenter, $database, $text) {

        //find tags
        
        $tags = $this->findTags($text);
        
        //parse each tag
        
        $parsedTags = array();
        foreach ($tags as $tagEntry) {
            $parsedTag = $this->parseTag($database, $tagEntry);
            if ($parsedTag) {
                $parsedTags[$tagEntry] = $parsedTag;
            }
        }
        
        //replace tags in texxt
        
        $tagDropHolder = array();
        
        foreach ($parsedTags as $parsedTagInput => $parsedTag) {
            $text = $this->applyTag($presenter, $parsedTagInput, $parsedTag, $text, $tagDropHolder);
        }
        
        //put tagDropDowns on end of text
        
        foreach($tagDropHolder as $tagDrop) {
            $text .= $tagDrop;
        }

        return $text;
    }

    private function findTags($text) {

        $tags = array();

        $tags_out = array();

        if (preg_match_all(self::HASHTAG_REGEX, $text, $tags_out)) {

            foreach ($tags_out[0] as $tagEntry) {

                while (Strings::contains($tagEntry, '#')) {
                    $tagEntry = substr($tagEntry, 1);
                }

                if (!in_array($tagEntry, $tags)) {
                    $tags[] = $tagEntry;
                }
            }
        }

        return $tags;
    }

    private function parseTag($database, $tagEntry) {

        $tagOut = new \stdClass();

        $tagOut->tag = $tagEntry;
        $tagOut->words = 0;

        if (Strings::startsWith($tagEntry, '[') && Strings::contains($tagEntry, ']')) {

            $entryDataString = substr($tagEntry, 1, strpos($tagEntry, ']')-1);
            $entryData = explode(";", $entryDataString);

            $tagOut->tag = $entryData[0];
            
            if (count($entryData) >= 2) {
                $tagOut->words = (int) $entryData[1];
            } else {
                $tagOut->words = 1;
            }
            
            if ($tagOut->words < 1) {
                $tagOut->words = 1;
            }
        }

        $tagId = $this->getTagId($database, $tagOut->tag);

        if (!$tagId) {
            return false;
        }

        //load pages by tag id
        $pagesQuery = $database->table('mysql_pages')
                ->where("order > ?", 0)
                ->order('order DESC')
                ->where(":mysql_pages_tags.page_id = id")
                ->where(":mysql_pages_tags.tag_id = ?", $tagId);

        $tagOut->pages = $pagesQuery;

        return $tagOut;
    }

    private function getTagId($database, $tag) {

        $tagSel = $database->table('mysql_tags')->where('tag', $tag)->fetch();

        if ($tagSel) {
            return $tagSel->id;
        }

        return false;
    }

    private function applyTag(\Nette\Application\UI\Presenter $presenter, $parsedTagInput, $parsedTag, $text, &$tagDropHolder) {

        if ($parsedTag->words == 0) {

            $buildedTag = $this->buildTagString($presenter, $parsedTag, $parsedTagInput, $tagDropHolder);

            $text = str_replace('#' . $parsedTagInput, $buildedTag, $text);
        } else {

            while (Strings::contains($text, $parsedTagInput)) {

                $tagLoc = strpos($text, $parsedTagInput);
                $tagEnd = $this->findMoreWordsTagEnd($text, $parsedTag->words, $tagLoc);

                // /*DEBUG*/ echo $tagLoc.'>>'.$tagEnd.'('.($tagEnd - $tagLoc).') = |'.  substr($text, $tagLoc, $tagEnd - $tagLoc).'|<br />';

                $tagInputStart = strpos($text, ']', $tagLoc) + 1;
                $buildedTag = $this->buildTagString($presenter, $parsedTag, substr($text, $tagInputStart, $tagEnd - $tagInputStart), $tagDropHolder);

                $text = substr_replace($text, $buildedTag, $tagLoc - 1, $tagEnd - $tagLoc + 1);
            }
        }
        
        return $text;
    }

    private function findMoreWordsTagEnd($text, $words, $tagStart) {
        $wordsCounter = 0;
        $tagEnd = $tagStart;

        while ($wordsCounter < $words) {
            $tagEnd++;
            if ($text[$tagEnd] == ' ' || $text[$tagEnd] == '<') {
                $wordsCounter++;
            }
        }

        return $tagEnd;
    }

    private function buildTagString(\Nette\Application\UI\Presenter $presenter, $parsedTag, $input, &$tagDropHolder) {

        if ($parsedTag->pages->count() == 1) {

            $page = $parsedTag->pages->fetch();

            return '<a href="' . $presenter->link("Homepage:show", $page->id) . '">' . $input . '</a>';
        } else {

            $out = '<a data-dropdown="tag-drop-' . $parsedTag->tag . '" aria-controls="tag-drop-' . $parsedTag->tag . '" aria-expanded="false">' . $input . '</a>';
            
            if(!isset($tagDropHolder[$parsedTag->tag])) {
                
                $outTagDrop = '<ul id="tag-drop-' . $parsedTag->tag . '" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">';
                foreach ($parsedTag->pages as $page) {
                    $outTagDrop .= '<li><a href="' . $presenter->link("Homepage:show", $page->id) . '">' . $page->title . '</a></li>';
                }
                $outTagDrop .= '</ul>';

                $tagDropHolder[$parsedTag->tag] = $outTagDrop;
            }
            
            return $out;
        }
    }

}
