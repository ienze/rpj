$.fn.boxResizer = function (settingsIn) {

    var boxResizer = this;

    var settings = $.extend({}, {
        stableTime: 300,
        animTime: 300,
        ajax_url: 'http://' + document.domain
    }, settingsIn);

    var enabled = false;
    var animating = false;
    
    var enable = function() {
      
      boxResizer.find("li .panel").click(function (event) {
  
          if (animating)
              return;
  
          if ($(event.target).is("a"))
              return;
  
          var panel = $(this);
          var enlarge = !panel.hasClass('boxResized');
  
          animating = true;
  
          var openedBefore = panel.parent().parent().find('.boxResized');
  
          openedBefore.each(function () {
              var panel = $(this);
              var pageId = panel.data('page-id');
              hideContent(panel, pageId, false);
          });
  
          openedBefore.removeClass('boxResized', 1000, function () {
              animating = false;
  
              if (panel.parent().index() === openedBefore.parent().index())
                  openedBefore.css('z-index', '');
  
              hideContent(panel, pageId, true);
          });
  
          if (panel.parent().index() !== openedBefore.parent().index())
              openedBefore.css('z-index', '');
  
  
          if (enlarge) {
  
              var pageId = panel.data('page-id');
  
              var panelFullContent = panel.find('.fullContent');
              if (panelFullContent.size() === 0) {
                  loadContent(panel, pageId);
              }
  
              var i = panel.parent().index();
  
  
              if (i % 3 === 0)
                  panel.addClass('on-column-left');
              else if (i % 3 > 1)
                  panel.addClass('on-column-right');
              else
                  panel.addClass('on-column-center');
  
              showContent(panel, pageId, false);
  
              panel.css('z-index', '6');
              panel.addClass('boxResized', 1000, function () {
                  animating = false;
  
                  if (panelFullContent.size() > 0) {
                      showContent(panel, pageId, true);
                  }
              });
          }
      });
    };
    
    var disable = function() {
      
        boxResizer.find("li .panel").off("click");
      
    };
    
    var checkSize = function() {
      
      if($(window).width() > 1024) {
          if(!enabled) {
              enable();
              enabled = true;
          }
      } else {
          if(enabled) {
              disable();
              enabled = false;
          }
      }
      
    }
    
    $(window).resize(function() {
        checkSize();    
    });
    checkSize();
    
    function loadContent(panel, pageId) {

        showContent(panel, pageId, false);

        $.ajax({
            url: settings.ajax_url + "/clanok/" + pageId,
            dataType: 'json'
        }).done(function (data) {

            var panelFullContent = $('<div class="fullContent"></div>');
            panel.find('.panel-content').append(panelFullContent);

            panelFullContent.html(data.content);
            panelFullContent.foundation();

            showContent(panel, pageId, true);
        });
    }

    function showContent(panel, pageId, done) {
        if (done) {
            panel.find('.fullContent').show();
        } else {
            panel.find('.excerpt').hide();
        }
    }

    function hideContent(panel, pageId, done) {
        if (done) {
            panel.find('.excerpt').show();
        } else {
            panel.find('.fullContent').hide();
        }
    }
};
